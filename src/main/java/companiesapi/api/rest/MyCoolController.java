package companiesapi.api.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coolstuff")
public class MyCoolController {

    @GetMapping("/supercool")
    public String doCoolStuff() {
        return "Something cool!";
    }
}
