INSERT INTO `company` (`id`, `name`, `logo`, `established`, `employees`) VALUES
	(1, 'Marek Global International', 'https://image.shutterstock.com/image-vector/cute-penguin-icon-flat-style-600w-744369634.jpg', '2019-11-14', 12334345),
	(2, 'LHV', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV', '2005-01-21', 366),
	(5, 'Tallinna Sadam', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM', '1991-12-25', 487),
	(7, 'Tallink', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL', '2019-10-30', 345);
